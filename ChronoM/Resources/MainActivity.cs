﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util;

namespace ChronoM
{
	[Activity (Label = "ChronoM", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		TextView timeView;
		Button startStopButton;
		Button resetButton;
	
		ISharedPreferences data;
		private Handler handler;
		bool isStarted;
		long startTime = 0;

		protected override void OnCreate (Bundle bundle)
		{
			
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);

			handler = new Handler ();
			isStarted = false;
		
			timeView=(TextView)FindViewById(Resource.Id.timeView);


			startStopButton=(Button)FindViewById(Resource.Id.startStopButton);
			startStopButton.Text = Resources.GetText (Resource.String.start);
			startStopButton.Click += (sender, eventArgs) => ClickButton();

			resetButton=(Button)FindViewById(Resource.Id.resetButton);
			resetButton.Text = Resources.GetText (Resource.String.reset);
			resetButton.Click += (sender, eventArgs) => Reset();


			data = this.GetSharedPreferences (GetString (Resource.String.file_name), FileCreationMode.Private);
			ShowChrono ();
			Log.Debug("Create", "OnCreate called");


		}




		override protected void  OnStart() {
			base.OnStart ();
			Log.Debug("Start", "OnStart called");
		}

		override protected void OnResume()
		{
			base.OnResume(); // Always call the superclass first.
			startTime=data.GetLong (GetString (Resource.String.count), startTime);
			isStarted = data.GetBoolean (GetString (Resource.String.isStarted),isStarted);
			startStopButton.Text = data.GetString (GetString (Resource.String.saveText),startStopButton.Text);
			Log.Debug("Resume", "OnResume called");

		}

		override protected void OnPause()
		{
			base.OnPause (); // Always call the superclass first

			ISharedPreferencesEditor editor = data.Edit ();
			editor.PutLong (GetString (Resource.String.count), startTime);
			editor.PutBoolean (GetString (Resource.String.isStarted), isStarted);
			if (isStarted) {
				editor.PutString (GetString (Resource.String.saveText), GetString(Resource.String.start));
			}
			else {
				editor.PutString (GetString (Resource.String.saveText),  GetString(Resource.String.stop));
			}
		
			editor.Commit();
			Log.Debug("OnPause", "OnPause called");


		}



		override protected void OnStop(){
			base.OnStop ();
			Log.Debug("OnStop", "OnStop called");
		}
		override protected void OnDestroy(){
			base.OnDestroy ();
			Log.Debug ("OnDestroy", "OnDestroy Called");
		}


		public void Run() {
			startTime = startTime+500;

			ShowChrono ();
			handler.PostDelayed(Run, 500);
		}

		public void ShowChrono (){
			int seconds = (int) (startTime / 1000);
			int minutes = seconds / 60;
			seconds = seconds % 60;


			timeView.Text=minutes+": " +seconds;
		
		}
		public void ClickButton(){
			if (isStarted) {
				StopChrono ();
				handler.RemoveCallbacksAndMessages (null);

			} else {
				isStarted = true;
				handler.PostDelayed(Run, 0);
				startStopButton.Text = Resources.GetText (Resource.String.stop);

			}


		}


		public void Reset(){
			startTime = 0;

			StopChrono ();

			handler.RemoveCallbacksAndMessages (null);
			ShowChrono ();

		
		}
		public void StopChrono(){
			startStopButton.Text = Resources.GetText (Resource.String.start);
			isStarted = false;

		}



	}
}


